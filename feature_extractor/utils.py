import random
import numpy as np

from torch.utils.data.sampler import Sampler
import torch as th
import math
from collections import defaultdict
import torch
from sklearn.model_selection import StratifiedKFold
import cv2

class EqualWeightedSampler(Sampler):
    def __init__(self, y):
        self.y = y
        self.sample_map = defaultdict(list)
        for idx, label in enumerate(y):
            self.sample_map[label].append(idx)

    def __iter__(self):
        self.count = 0
        while self.count < len(self.y):
            chosen_class = np.random.choice(list(self.sample_map.keys()))
            yield np.random.choice(self.sample_map[chosen_class])

    def __len__(self):
        return len(self.y)

def show_cam_on_image(img: np.ndarray,
                      mask: np.ndarray,
                      use_rgb: bool = False,
                      mask_intensity: float = 1.0,
                      colormap: int = cv2.COLORMAP_JET) -> np.ndarray:
    """ This function overlays the cam mask on the image as an heatmap.
    By default the heatmap is in BGR format.
    :param img: The base image in RGB or BGR format.
    :param mask: The cam mask.
    :param use_rgb: Whether to use an RGB or BGR heatmap, this should be set to True if 'img' is in RGB format.
    :param colormap: The OpenCV colormap to be used.
    :returns: The default image with the cam overlay.
    """
    heatmap = cv2.applyColorMap(np.uint8(255 * mask), colormap)
    if use_rgb:
        heatmap = cv2.cvtColor(heatmap, cv2.COLOR_BGR2RGB)
    heatmap = np.float32(heatmap) / 255

    if np.max(img) > 1:
        raise Exception(
            "The input image should np.float32 in the range [0, 1]")

    cam = mask_intensity*heatmap + img
    cam = cam / np.max(cam)
    return np.uint8(255 * cam)