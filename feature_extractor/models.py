import os
import torch
from torch import nn
import torch.nn.functional as F
from torchvision import transforms
from torch.utils.data import DataLoader, random_split
import pytorch_lightning as pl
import torchvision.models as models
import pdb
import numpy as np
from torch.utils.data import Dataset
import os
from PIL import Image
from torchvision import transforms as tfs
import pandas as pd
import matplotlib.pyplot as plt
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping, LearningRateMonitor
import torchmetrics
from sklearn.metrics import roc_auc_score
from pytorch_lightning.loggers import WandbLogger
import wandb
from dataset import ImageDataset, ImageDataModule
import argparse
import multiprocessing
from torch.optim.lr_scheduler import ReduceLROnPlateau
import imageio
import cv2
from utils import *


class Model(pl.LightningModule):
    def __init__(self,
                lr=1e-4,
                val_check_interval=5000,
                use_context=1, 
                num_classes=16,
                num_inputs=1,
                use_metrics=None,
                inject_exp=0,
                use_regression=False,
                get_embedding=False):
        super().__init__()
        self.save_hyperparameters()
        self.lr = lr
        self.val_check_interval = val_check_interval
        self.channels_per_image = 3 if use_context else 1
        self.num_inputs = num_inputs
        self.num_input_channels = self.num_inputs * self.channels_per_image
        self.get_embedding = get_embedding
        self.use_regression = use_regression
        self.inject_exp = inject_exp
        backbone = models.resnet50(pretrained=True)
        num_filters = backbone.fc.in_features
        if self.inject_exp:
            num_filters += self.num_inputs
        layers = list(backbone.children())[:-1]
        #pdb.set_trace()

        if self.num_input_channels > 3:
            # pdb.set_trace()
            temp = torch.nn.Conv2d(self.num_input_channels, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
            if self.channels_per_image == 3:
                temp.weight = torch.nn.Parameter(layers[0].weight.repeat(1, self.num_inputs, 1, 1))
            else:
                temp.weight = torch.nn.Parameter(layers[0].weight.repeat(1, self.num_input_channels//3+1, 1, 1)[:, 0:self.num_input_channels, :, :])
            layers[0] = temp
        # pdb.set_trace()
        self.feature_extractor = nn.Sequential(*layers)
        
        self.num_classes = num_classes
        self.classifier = nn.Linear(num_filters, self.num_classes)
        if self.use_regression:
            self.loss_fn = torch.nn.L1Loss()
        else:
            self.loss_fn = torch.nn.CrossEntropyLoss()

        self.metric_names = use_metrics
        self.metric_funcs = {}
        self.one_hot_to_int_func = lambda x: torch.argmax(x, dim=1)
        for metric in self.metric_names:
            # if metric == "auc":
            #     self.metric_funcs[metric] = torchmetrics.AUROC(
            #         num_classes=self.num_classes, average='macro')
            # elif metric == "map":
            #     self.metric_funcs[metric] = torchmetrics.AveragePrecision(
            #         num_classes=self.num_classes)
            # elif metric == "precision":
            #     self.metric_funcs[metric] = torchmetrics.Precision(
            #         num_classes=self.num_classes,
            #         average='weighted',
            #         top_k=1
            #     )
            # elif metric == "recall":
            #     self.metric_funcs[metric] = torchmetrics.Recall(
            #         num_classes=self.num_classes,
            #         average='weighted',
            #         top_k=1
            #     )
            # elif metric == "specificity":
            #     self.metric_funcs[metric] = torchmetrics.Specificity(
            #         num_classes=self.num_classes,
            #         average='weighted',
            #         top_k=1
            #     )
            if metric == "f1":
                self.monitor_metric = 'val_f1'
                self.metric_funcs[metric] = torchmetrics.F1(
                    num_classes=self.num_classes,
                    average='weighted',
                    top_k=1
                )
            elif metric == "r2":
                self.monitor_metric = 'val_r2'
                self.metric_funcs[metric] = torchmetrics.R2Score(
                    num_outputs=self.num_classes,
                    multioutput='uniform_average'
                )

    def forward(self, x):
        if self.get_embedding:
            x = self.feature_extractor(x).flatten(1)
        elif self.inject_exp:
            x_img = x[0]
            x_tab = x[1]
            x = torch.cat((self.feature_extractor(x_img).flatten(1), x_tab), dim=1)
            x = self.classifier(x)
        else:
            x = self.classifier(self.feature_extractor(x).flatten(1))
        return x

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        return {
            "optimizer": optimizer,
            "lr_scheduler": {
                "scheduler": ReduceLROnPlateau(optimizer,
                                               mode="max",
                                               factor=0.2,
                                               patience=15,
                                               min_lr=1e-7,
                                               ),
                "monitor": self.monitor_metric,
                "strict": False,
                "interval": "step",
                "frequency": self.val_check_interval,
                "name": "learning_rate"
            }
        }

    def loss(self, logits, labels):
        #pdb.set_trace()
        return self.loss_fn(logits, labels)

    def metric(self, preds, targets):
        metric_score = self.metric_func(preds, targets)
        return torch.mean(torch.stack(metric_score))

    def _step(self, batch):
        x, y = batch
        y_hat = self(x)
        if self.use_regression:
            y_true = y.type(torch.float32)
        else:
            y_true = y.flatten().type(torch.long)
        return y_hat, y_true

    def training_step(self, batch, batch_idx):
        y_hat, y_true = self._step(batch)
        loss = self.loss(y_hat, y_true)
        self.log('train_loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        y_hat, y_true = self._step(batch)
        loss = self.loss(y_hat, y_true)
        self.log("val_loss", loss, on_epoch=True)
        return (y_true, y_hat)

    def validation_epoch_end(self, outputs):
        targets = torch.cat([x[0] for x in outputs]).type(torch.uint8)
        preds = torch.cat([x[1] for x in outputs])
        for name, func in self.metric_funcs.items():
            metric_score = func(preds.cpu(), targets.cpu())
            self.log("val_{}".format(name), metric_score)

    def test_step(self, batch, batch_idx):
        y_hat, y_true = self._step(batch)
        return (y_true, y_hat)

    def predict_step(self, batch, batch_idx):
        return self.test_step(batch, batch_idx)

    def test_epoch_end(self, outputs):
        targets = torch.cat([x[0] for x in outputs]).type(torch.uint8)
        preds = torch.cat([x[1] for x in outputs])
        for name, func in self.metric_funcs.items():
            metric_score = func(preds.cpu(), targets.cpu())
            self.log("test_{}".format(name), metric_score)
