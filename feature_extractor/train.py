import os
import torch
from torch import nn
import torch.nn.functional as F
from torchvision import transforms
from torch.utils.data import DataLoader, random_split
import pytorch_lightning as pl
import torchvision.models as models
import pdb
import numpy as np
from torch.utils.data import Dataset
import os
from PIL import Image
from torchvision import transforms as tfs
import pandas as pd
import matplotlib.pyplot as plt
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping, LearningRateMonitor
import torchmetrics
from sklearn.metrics import roc_auc_score
from pytorch_lightning.loggers import WandbLogger
import wandb
from dataset import ImageDataset, ImageDataModule
import argparse
import multiprocessing
from torch.optim.lr_scheduler import ReduceLROnPlateau
import imageio
import cv2
from utils import *
from models import *


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--run_name', type=str)
    parser.add_argument('--mode', type=str, default="train")
    parser.add_argument('--resume', type=str)
    parser.add_argument('--batch_size', type=int, default=32)
    parser.add_argument('--val_check_interval', type=int, default=5000)
    parser.add_argument('--use_regression', type=int, default=0)
    parser.add_argument('--num_classes', type=int, default=0)
    parser.add_argument('--data_version', type=int, default=13)
    parser.add_argument('--lr', type=float, default=1e-5)
    parser.add_argument('--use_context', type=int, default=1)
    parser.add_argument('--inject_exp', type=int, default=0)
    parser.add_argument('--study_name', type=str, default='UPMC')
    parser.add_argument('--gpu_id', type=int, default=0)

    args = parser.parse_args()

    wandb_logger = WandbLogger(name=args.run_name, 
        project='dapi', log_model=True, reinit=True, id=args.run_name)

    """ Set configuration variables """
    root_dir = "/home/ubuntu/efs/dapi/resnet/"
    
    data_path = '{}dfs/version_{}/data_df.pqt'.format(
        root_dir, args.data_version)
    data_df = pd.read_parquet(data_path)

    # input_biomarkers = ['DAPI', 'CD45RA', 'CD15', 'PanCK', 'HLA-DR', 'Ki67', 'Vimentin']
    input_biomarkers = ['DAPI', 'CD4', 'CD15', 'PanCK', 'CD8', 'Ki67', 'Vimentin']

    study_name = args.study_name.upper()
    if args.use_regression == 0:
        label = 'LABEL'
        num_classes = len(data_df[label].unique()) if args.num_classes == 0 else args.num_classes
        metrics = ['f1']
        monitor_metric = 'val_f1'
    else:
        label = ['CD68','PD1','Podoplanin','CollagenIV',
        'aSMA','CD49f','CD8','CD45RO','TMEM16A','CD31',
        'CD21','CD163','CD3e','CD47','CD11c','CD20',
        'FoxP3','PDL1','CD38','CD57','p16','CD45','CD4',
        'GranzymeB','CD56','CD134','ICOS','CD16','CD11b',
        'CD117','CD69','CD152','CD14','CD34']
        num_classes = len(label)
        metrics = ['r2']
        monitor_metric = 'val_r2'

    data = ImageDataModule(data_df=data_df,
                           use_context=args.use_context,
                           label=label,
                           input_biomarkers=input_biomarkers,
                           study_name=study_name,
                           batch_size=args.batch_size,
                           inject_exp=args.inject_exp,
                           use_regression=args.use_regression,
                           workers=4#multiprocessing.cpu_count()
                           )
    wandb_obj = wandb_logger.experiment

    trainer = pl.Trainer(gpus=[args.gpu_id],
                         log_every_n_steps=100,
                         resume_from_checkpoint=args.resume,
                         max_steps=1000000,
                         val_check_interval=args.val_check_interval,
                         benchmark=True,
                         default_root_dir=root_dir,
                         checkpoint_callback=True,
                         logger=wandb_logger,
                         callbacks=[ModelCheckpoint(monitor=monitor_metric,
                                                    save_last=True,
                                                    every_n_train_steps=args.val_check_interval,
                                                    mode="max",
                                                    auto_insert_metric_name=False,
                                                    filename='epoch={epoch:02d}-step={step}-'+monitor_metric+'={'+monitor_metric+':.3f}',
                                                    save_top_k=1),
                                    EarlyStopping(monitor=monitor_metric,
                                                  min_delta=0.001,
                                                  patience=15,
                                                  verbose=True,
                                                  mode="max",
                                                  check_on_train_epoch_end=False),
                                    LearningRateMonitor()
                                    ])
    if args.mode == 'train':
        #pdb.set_trace()
        wandb_obj.config.update(args, allow_val_change=True)
        wandb_obj.save("./train.py")
        wandb_obj.save("./dataset.py")
        wandb_obj.save("./utils.py")
        wandb_obj.save("./models.py")

        model = Model(lr=args.lr,
                         val_check_interval=args.val_check_interval,
                         num_classes=num_classes,
                         use_context=args.use_context,
                         use_regression=args.use_regression,
                         num_inputs=len(input_biomarkers),
                         use_metrics=metrics,
                         inject_exp=args.inject_exp)
        wandb_logger.watch(model)
        trainer.fit(model, data)
        trainer.test(test_dataloaders=data.test_dataloader())
    elif args.mode == 'test':
        model = Model.load_from_checkpoint(args.resume)
        print("Testing model...")
        wandb_logger.watch(model)
        trainer.test(model, datamodule=data)
    elif args.mode == 'embed':
        model = Model.load_from_checkpoint(args.resume, get_embedding=True)
        data.setup()
        dataloader = data.predict_dataloader()
        device = torch.device("cuda:0")
        model.to(device)
        #pdb.set_trace()
        batch = []
        path = '/home/ubuntu/efs/dapi/resnet/dapi/{}/'.format(args.run_name)
        path += 'embeddings_{}'.format(args.embed_name)
        os.makedirs(path, exist_ok=True)
        num_batches = len(dataloader)
        for batch_idx, data in enumerate(dataloader):
            if batch_idx % 1000 == 0:
                print(batch_idx, '{:.2f}%'.format(batch_idx*1.0/num_batches))
            data = data[0].to(device)
            #pdb.set_trace()
            batch.append(model(data).detach().cpu().numpy())
            if len(batch) == 1000:
                #pdb.set_trace()
                np.save('{}/{}.npy'.format(path, batch_idx), batch)
                batch = []

            if batch_idx == num_batches-1:
                np.save('{}/{}.npy'.format(path, batch_idx), batch)
                print("Done saving!")

    elif args.mode == 'predict':
        # pdb.set_trace()
        model = Model.load_from_checkpoint(args.resume,
                                    num_inputs=len(input_biomarkers),
                                    use_context=args.use_context, 
                                    num_classes=num_classes,
                                    use_regression=args.use_regression,
                                    use_metrics=metrics,
                                    inject_exp=args.inject_exp,
                                    get_embedding=False)
        print("Running predictions for the entire dataset...")
        predictions = trainer.predict(model,
                                      datamodule=data,
                                      return_predictions=True,
                                      ckpt_path=args.resume
                                      )
        targets = torch.cat([x[0] for x in predictions]).cpu().numpy()
        preds = torch.cat([x[1] for x in predictions]).cpu().numpy()
        save_dir = root_dir + "dapi/" + args.run_name
        os.makedirs(save_dir, exist_ok=True)
        np.save(os.path.join(save_dir, "preds_all.npy"), preds)
        np.save(os.path.join(save_dir, "targets_all.npy"), targets)
        print("Finished saving predictions")

    elif args.mode == 'grad':
        from pytorch_grad_cam import GradCAM, ScoreCAM, GradCAMPlusPlus, AblationCAM, XGradCAM, EigenCAM
        from pytorch_grad_cam.utils.image import show_cam_on_image
        device = torch.device("cuda:0")

        model = Model.load_from_checkpoint(args.resume, num_inputs=len(input_biomarkers))
        model.to(device)

        target_layers = model.feature_extractor[7][-1]#[model.layer4[-1]]
        #input_tensor = # Create an input tensor image for your model..
        # Note: input_tensor can be a batch tensor with several images!

        data.setup()
        dataloader = data.test_dataloader()
        # for batch_idx in range(10):
        batch = next(iter(dataloader))#.to(device)
        input_tensor = batch[0].to(device)
        labels = batch[1]
        preds = model(input_tensor).detach().cpu().numpy()
        #input_tensor = data[0].to(device)
        #pdb.set_trace()
        # batch = []
        # os.makedirs(path, exist_ok=True)
        # num_batches = len(dataloader)
        # for batch_idx, data in enumerate(dataloader):
        #     if batch_idx % 1000 == 0:
        #         print(batch_idx)
        #     data = data[0].to(device)

        # Construct the CAM object once, and then re-use it on many images:
        #pdb.set_trace()
        cam = GradCAM(model, target_layers, use_cuda=1)

        # You can also pass aug_smooth=True and eigen_smooth=True, to apply smoothing.
        grayscale_cam = cam(input_tensor=input_tensor, eigen_smooth=True, target_category=None)
        rgb_img = input_tensor.detach().cpu().numpy()
        # In this example grayscale_cam has only one image in the batch:

        grads = []
        for i in range(len(grayscale_cam)):
            mask = grayscale_cam[i, :]
            img = np.moveaxis(rgb_img[i, :], 0, -1)
            grads.append((mask, img))

        path = '/home/ubuntu/efs/dapi/resnet/dapi/{}/'.format(args.run_name)
        path += 'grads'
        os.makedirs(path, exist_ok=True)
        np.save(os.path.join(path, 'grad.npy'), [grads, preds, labels])
        #grad = show_cam_on_image(img, mask, use_rgb=True)

    elif args.mode == 'ssl':
        from pl_bolts.models.self_supervised import SimCLR
        from pl_bolts.models.self_supervised.simclr import SimCLREvalDataTransform, SimCLRTrainDataTransform

        model = SimCLR.load_from_checkpoint('imagenet_weights/simclr_imagenet.ckpt', strict=False)

        model = model.encoder
        model.eval()

        data.setup()
        dataloader = data.predict_dataloader()
        device = torch.device("cuda:0")
        model.to(device)
        batch = []
        path = '/home/ubuntu/efs/dapi/resnet/dapi/{}/'.format(args.run_name)
        path += 'simclr_{}'.format(input_biomarkers[0])
        os.makedirs(path, exist_ok=True)
        num_batches = len(dataloader)
        for batch_idx, data in enumerate(dataloader):
            if batch_idx % 1000 == 0:
                print(batch_idx)
            data = data[0].to(device)
            #pdb.set_trace()
            batch.append(model(data)[0].detach().cpu().numpy())
            if len(batch) == 1000:
                #pdb.set_trace()
                np.save('{}/{}.npy'.format(path, batch_idx), batch)
                batch = []

            if batch_idx == num_batches-1:
                np.save('{}/{}.npy'.format(path, batch_idx), batch)
                print("Done saving!")
    elif args.mode == 'experiment':
        data.setup()
        dataloader = data.predict_dataloader()
        batch = []
        num_batches = len(dataloader)
        for batch_idx, data in enumerate(dataloader):
            pass