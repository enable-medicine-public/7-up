from PIL import Image
from torchvision import transforms as tfs
import os
import torch
import pytorch_lightning as pl
from torch.utils.data import Dataset
from torch.utils.data import DataLoader, random_split, WeightedRandomSampler
import pandas as pd
import pdb
from utils import *
# import swifter

class ImageDataset(Dataset):
    def __init__(self,
                 df,
                 path_map=None,
                 use_context=1, 
                 input_biomarkers=None, 
                 mode=None, 
                 label=None, 
                 inject_exp=0,
                 study_name=None):
        self._image_paths = []
        self._labels = []
        self._mode = mode
        self._df = self.get_split(df, self._mode)
        self.study_name = study_name
        self.data_versions = {'DAPI': 0,
                        'Ki67': 0,
                        'CD8': 0,
                        'CD15': 0,
                        'CD20': 0,
                        'PanCK': 0,
                        'FoxP3': 0,
                        'CD4': 0,
                        'PD1': 0,
                        'HLA-DR': 0,
                        'Vimentin': 0,
                        'CD45RA': 0
                        }
        self.use_context = use_context
        self.input_biomarkers = input_biomarkers
        self.inject_exp = inject_exp
        if label is None:
            label = 'LABEL'

        self._image_paths = self._df['PATH'].values
        if self.inject_exp:
            self._input_expressions = self._df[self.input_biomarkers].astype(np.float32).values
        self._labels = self._df[label].values
        self._num_images = len(self._image_paths)

        self.normalize = tfs.Normalize(mean=[0.485, 0.456, 0.406],
                                  std=[0.229, 0.224, 0.225])

        if self._mode == 'train':
            self.img_aug = tfs.Compose([
            tfs.RandomHorizontalFlip(p=0.5),
            tfs.RandomVerticalFlip(p=0.5),
            tfs.RandomAffine(degrees=45, translate=(0.1, 0.1), scale=(0.8, 1.2)),
            tfs.ToTensor(),
            #self.normalize,
            ])
        else:
            self.img_aug = tfs.Compose([
            tfs.ToTensor(),
            #self.normalize,
            ])

    def get_weights(self):
        return self._weights

    def get_labels(self):
        return self._labels

    def get_split(self, df, mode):
        if mode in ['train', 'valid', 'test']:
            # if mode == 'train':
            #     return df[~df.PATH.str.contains('c003') & ~df.PATH.str.contains('c005')]
            # elif mode == 'valid':
            #     return df[df.PATH.str.contains('c003')]
            # elif mode == 'test':
            #     return df[df.PATH.str.contains('c005')]
            # else:
            #     return df[df.split == mode]
            return df[df.split == mode]
        elif mode == 'all':
            return df.drop_duplicates()
        else:
            raise ValueError("Not a valid mode") 

    def __len__(self):
        return self._num_images

    def __getitem__(self, idx):

        images = []
        try:
            for biomarker in self.input_biomarkers:
                data_version = self.data_versions[biomarker] if biomarker in self.data_versions else 0
                path = self._image_paths[idx].format(self.study_name, biomarker, data_version)
                image = Image.open(path)
                if self.use_context == 0:
                    image = np.array(image)[:,:,0]
                    image = Image.fromarray(image)
                image = self.img_aug(image)
                images.append(image)
        except:
            path = self._image_paths[idx].format(self.study_name, biomarker, self.data_versions[biomarker])
            #print("Error {}, trying another image...".format(path))
            print(path)
            return self.__getitem__(np.random.choice(self._num_images))
        X = torch.cat(images, 0)
        if self.inject_exp:
            X = (X, self._input_expressions[idx])
        labels = self._labels[idx]
        return (X, labels)


class ImageDataModule(pl.LightningDataModule):
    def __init__(self, 
                 use_context=1, 
                 use_regression=0,
                 input_biomarkers=None, 
                 data_df=None, 
                 study_name=None, 
                 label=None,
                 inject_exp=0,
                 batch_size=32, 
                 workers=4):
        super().__init__()
        self.data_df = data_df
        # self.path_map = path_map
        self.input_biomarkers = input_biomarkers
        self.batch_size = batch_size
        self.workers = workers
        self.study_name = study_name
        self.use_context = use_context
        self.use_regression = use_regression
        self.label = label
        self.inject_exp = inject_exp
        self.kwargs = {
            "use_context": self.use_context,
            "input_biomarkers": self.input_biomarkers, 
            "study_name": self.study_name,
            "label": self.label,
            "inject_exp": self.inject_exp
            }

    def setup(self, stage=None):
        pass
        # self.train_ds = ImageDataset(self.data_df, mode='train', **kwargs)
        # self.val_ds = ImageDataset(self.data_df, mode='valid', **kwargs)
        # self.test_ds = ImageDataset(self.data_df, mode='test', **kwargs)
        # self.pred_ds = ImageDataset(self.data_df, mode='all', **kwargs)

    def train_dataloader(self):
        self.train_ds = ImageDataset(self.data_df, mode='train', **self.kwargs)
        if self.use_regression == 0:
            sampler = EqualWeightedSampler(self.train_ds.get_labels())
        else:
            sampler = None
        return DataLoader(
            self.train_ds,
            batch_size=self.batch_size, num_workers=self.workers,
            drop_last=True, sampler=sampler, pin_memory=True)

    def val_dataloader(self):
        self.val_ds = ImageDataset(self.data_df, mode='valid', **self.kwargs)
        return DataLoader(
            self.val_ds,
            batch_size=self.batch_size*8, num_workers=self.workers,
            drop_last=False, shuffle=False, pin_memory=True)

    def test_dataloader(self):
        self.test_ds = ImageDataset(self.data_df, mode='test', **self.kwargs)
        return DataLoader(
            self.test_ds,
            batch_size=self.batch_size, num_workers=self.workers,
            drop_last=False, shuffle=False, pin_memory=True)

    def predict_dataloader(self):
        self.pred_ds = ImageDataset(self.data_df, mode='all', **self.kwargs)

        return DataLoader(
            self.pred_ds,
            batch_size=self.batch_size, num_workers=self.workers,
            drop_last=False, shuffle=False, pin_memory=True)


