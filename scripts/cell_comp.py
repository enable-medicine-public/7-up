import multiprocessing
from utils import *

# Load dataframes

max_samples = 100000
data_version = 13
dataset = 'upmc'
run_name = '1012_7c_ca'
n_dims = 15

cell_exp_df = pd.read_parquet('../dfs/UPMC/alex_norm_exp_df.pqt')
cell_labels = get_labels(dataset)
pred_cols = get_pred_cols(cell_labels)
all_biomarkers = get_biomarkers(dataset)

data_df = get_pred_df(data_version, run_name)
data_df = binarize_labels(data_df, cell_labels)

# Exclude invalid cell IDs
data_df = data_df[~data_df.CELL_ID.isin([15869, 89254, 108260, 1221615])]

# Get train/val/test dataframes

merged_df = data_df.merge(cell_exp_df, on="CELL_ID")
train_df, val_df, test_df = get_df_splits(merged_df)
test_df['ACQUISITION_ID'] = test_df.PATH.apply(lambda x: x.split('/')[6])

test_df_w_preds = pd.read_parquet('../export/upmc_celltype_preds_c005_120721_ensemble.pqt')
test_df_w_preds = test_df.merge(test_df_w_preds, on='CELL_ID', suffixes=['', '_pred'])

acq_ids = test_df['ACQUISITION_ID'].sample(30).unique()
sizes = [5, 10, 100, 500, 1000, 3000]

y_features = ['aSMA','CD117','CD11c','CD57','CD56','CD45RO','ICOS','CD31','TMEM16A','CD45','CD8','CD20','CD14','CD4','Podoplanin','CD152','FoxP3','PDL1','PD1','CD47','CD3e','CD16','CD163','CD11b','GranzymeB','CD49f','CD21','p16','CD68','CD69','CollagenIV','CD34','CD134','CD38']
#pdb.set_trace()
    
def exp_comp(acq_id):
    print(acq_id)
    cell_df = []
    data_df_ = test_df_w_preds[test_df_w_preds['ACQUISITION_ID'] == acq_id]
    img = get_segmask_from_acq_id(study_id='196a98d2-f0c1-4f22-b249-701dcbc3da84', acq_id=acq_id)

    for wh in sizes:
        print(wh)
        min_num_cells = 1
        num_y_tiles = int(np.ceil(img.shape[0]/wh))
        num_x_tiles = int(np.ceil(img.shape[1]/wh))
        tot_num_tiles = num_y_tiles * num_x_tiles

        for i in range(num_y_tiles):
            for j in range(num_x_tiles):
                y1,y2,x1,x2 = [i*wh, (i+1)*wh, j*wh, (j+1)*wh]
                patch = img[y1:y2, x1:x2]
                if patch.shape[0] < wh and patch.shape[1] < wh:
                    patch_full = np.zeros((wh, wh))
                    patch_full[0:patch.shape[0], 0:patch.shape[1]] = patch
                    patch = patch_full
                elif patch.shape[0] < wh:
                    patch = np.concatenate([patch, np.zeros((wh-patch.shape[0], wh))], axis=0)
                elif patch.shape[1] < wh:
                    patch = np.concatenate([patch, np.zeros((wh, wh-patch.shape[1]))], axis=1)
                cell_nums = np.unique(patch)[1:]
                num_cells = len(cell_nums)
                if num_cells < min_num_cells:
                    #print("Not enough cells")
                    continue
                cell_rows = data_df_[data_df_.CELL_ID.isin(cell_nums)]
                if len(cell_rows) == 0:
                    continue

                gt_exp = [np.mean(cell_rows[y]) for y in y_features]
                pred_exp = [np.mean(cell_rows[y+'_pred']) for y in y_features]

                cell_df.append([acq_id, wh, i, j, num_cells]+gt_exp+pred_exp)
    return cell_df

def cell_type_comp(acq_id):
    print(acq_id)
    cell_df = []
    data_df_ = test_df[test_df['ACQUISITION_ID'] == acq_id]
    img = get_segmask_from_acq_id(study_id='196a98d2-f0c1-4f22-b249-701dcbc3da84', acq_id=acq_id)
    for wh in sizes:
        print(wh)
        min_num_cells = 1
        num_y_tiles = int(np.ceil(img.shape[0]/wh))
        num_x_tiles = int(np.ceil(img.shape[1]/wh))
        tot_num_tiles = num_y_tiles * num_x_tiles

        for i in range(num_y_tiles):
            for j in range(num_x_tiles):
                y1,y2,x1,x2 = [i*wh, (i+1)*wh, j*wh, (j+1)*wh]
                patch = img[y1:y2, x1:x2]
                if patch.shape[0] < wh and patch.shape[1] < wh:
                    patch_full = np.zeros((wh, wh))
                    patch_full[0:patch.shape[0], 0:patch.shape[1]] = patch
                    patch = patch_full
                elif patch.shape[0] < wh:
                    patch = np.concatenate([patch, np.zeros((wh-patch.shape[0], wh))], axis=0)
                elif patch.shape[1] < wh:
                    patch = np.concatenate([patch, np.zeros((wh, wh-patch.shape[1]))], axis=1)
                cell_nums = np.unique(patch)[1:]
                num_cells = len(cell_nums)
                if num_cells < min_num_cells:
                    #print("Not enough cells")
                    continue
                cell_rows = data_df_[data_df_.CELL_ID.isin(cell_nums)]
                
                gt_dict = cell_rows['LABEL'].value_counts(normalize=True).to_dict()
                gt_comp = [gt_dict[i] if i in gt_dict else 0.0 for i in range(16)]
                pred_dict = cell_rows['preds_num'].value_counts(normalize=True).to_dict()
                pred_comp = [pred_dict[i] if i in pred_dict else 0.0 for i in range(16)]
                
                #score = scipy.stats.pearsonr(gt_comp, pred_comp)[0]
                cell_df.append([acq_id, wh, i, j, num_cells]+gt_comp+pred_comp)
    return cell_df

if __name__ == '__main__':
    pdb.set_trace()
    num_processes = 8
    pool = multiprocessing.Pool(num_processes)
    results = pool.map(exp_comp, acq_ids)
    outputs = []
    [outputs.extend(result) for result in results]
    pdb.set_trace()
    cells_df = pd.DataFrame(outputs, columns=['ACQUISITION_ID', 'PATCH_SIZE', 'ROW', 'COLUMN', 'NUM_CELLS']+['GT_{}'.format(i) for i in range(16)]+['PRED_{}'.format(i) for i in range(16)])
    cells_df.to_parquet('data/cell_comp_label_df.pqt')

#     cells_df = pd.DataFrame(outputs, columns=['ACQUISITION_ID', 'PATCH_SIZE', 'ROW', 'COLUMN', 'NUM_CELLS']+['GT_{}'.format(i) for i in y_features]+['PRED_{}'.format(i) for i in y_features])
#     cells_df.to_parquet('data/cell_comp_exp_df.pqt')
    print("DONE")