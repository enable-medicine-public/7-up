import os

os.environ["AWS_ACCESS_KEY"] = 
os.environ["AWS_SECRET_ACCESS_KEY"] = 
os.environ["SF_USERNAME"] = 
os.environ["SF_PASSWORD"] = 
os.environ["SF_ACCOUNT"] = 
os.environ["SF_WAREHOUSE"] = 
os.environ["SF_DATABASE"] = 
os.environ["SF_SCHEMA"] = 
# import warnings
# warnings.filterwarnings("ignore")
import boto3
import io
from snowflake import connector
import tifffile
import numpy as np
import scipy
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from collections import defaultdict
from sklearn.svm import SVC
from PIL import Image
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier, GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score, r2_score, \
balanced_accuracy_score, f1_score, confusion_matrix, \
accuracy_score, classification_report, average_precision_score, precision_recall_fscore_support
import sklearn.metrics as metrics
import matplotlib.pyplot as plt
import seaborn as sns
import gc
from sklearn.dummy import DummyClassifier
from sklearn.neural_network import MLPClassifier
import umap
from sklearn.cluster import *#AgglomerativeClustering
import matplotlib
import imageio
import json
from mpl_toolkits.axes_grid1 import ImageGrid
from scipy.stats import spearmanr
import multiprocessing
#import cuml
import scipy.stats as stats
import sklearn
#import lightgbm as lgb
from sklearn import multioutput
import pickle5 as pickle

import pdb

USER = os.environ.get('SF_USERNAME')
PASSWORD = os.environ.get('SF_PASSWORD')
ACCOUNT = os.environ.get('SF_ACCOUNT')
WAREHOUSE = os.environ.get('SF_WAREHOUSE')
DATABASE = os.environ.get('SF_DATABASE')
SCHEMA = os.environ.get('SF_SCHEMA')
ROLE = os.environ.get('SF_ROLE')

conn = connector.connect(
    user=USER,
    password=PASSWORD,
    account=ACCOUNT,
    warehouse=WAREHOUSE,
    database=DATABASE,
    schema=SCHEMA
)

cur = conn.cursor()
s3 = boto3.resource(
        's3',
        aws_access_key_id=os.environ.get('AWS_ACCESS_KEY'),
        aws_secret_access_key=os.environ.get('AWS_SECRET_ACCESS_KEY')
    )

def get_query_results(query):
    cur.execute(query)
    df_query = cur.fetch_pandas_all()
    return df_query

def get_cell_xy_from_acq_id(acq_id):
    cur.execute("""
        select DISTINCT 
        cbe.CELL_ID,
        cso.X,
        cso.Y
        from ZOU_LAB_DATA_EXPORT.CORE_DATA.CELL_BIOMARKER_EXPRESSIONS cbe

        join ZOU_LAB_DATA_EXPORT.CORE_DATA.CELL_SEGMENTATION_OUTPUT cso

        on 
         cbe.CELL_ID=cso.CELL_ID

        where 
            cbe.ACQUISITION_ID='{}' 
        and cbe.VERSION='4'
        and cbe.DS='2021-07-25'
        and cbe.CHANNEL='1'
        and cbe.CYCLE='2'
        and cso.ACQUISITION_ID='{}'
        and cso.VERSION='5'
        and cso.DS='2021-07-25'
    """.format(acq_id, acq_id))
    
    df_query = cur.fetch_pandas_all()
    return df_query

def get_exp_from_study_id(study_id, version=4):
    cur.execute("""
        select * from ZOU_LAB_DATA_EXPORT.CORE_DATA.CELL_BIOMARKER_EXPRESSIONS
            where DS='2021-12-09'
            and VERSION={}
            and STUDY_ID={}
            and BIOMARKER_NAME='DAPI'
    """.format(study_id, version))
    
    df_query = cur.fetch_pandas_all()
    return df_query

def get_dapi_from_study_id(study_id, version):
    cur.execute("""
        select * from ZOU_LAB_DATA_EXPORT.CORE_DATA.CELL_BIOMARKER_EXPRESSIONS
            where DS='2021-10-06'
            and STUDY_ID={}
            and version={}
            and CHANNEL=1
            and CYCLE=2
            and BIOMARKER_NAME='DAPI'
    """.format(study_id, version))
    
    df_query = cur.fetch_pandas_all()
    return df_query

def get_exp_from_study_id(study_id, version, cycle=2, channel=1):
    cur.execute("""
        select * from ZOU_LAB_DATA_EXPORT.CORE_DATA.CELL_BIOMARKER_EXPRESSIONS
            where DS='2021-10-06'
            and STUDY_ID={}
            and version={}
            and CHANNEL={}
            and CYCLE={}
    """.format(study_id, version, channel, cycle))
    
    df_query = cur.fetch_pandas_all()
    return df_query

def get_labels_from_study_id(study_id, version=8):
    cur.execute("""
        select
        ACQUISITION_ID,
        CELL_ID,
        CLUSTER_ID,
        CLUSTER_LABEL
        from ZOU_LAB_DATA_EXPORT.CORE_DATA.CELL_CLASSIFICATION_OUTPUT
        where DS='2021-08-30'
        and STUDY_ID={}
        and VERSION={}
    """.format(study_id, version))

    df_query = cur.fetch_pandas_all()
    return df_query

def get_img_from_acq_id(study_id, acq_id):
    fn = '/home/ubuntu/efs/dapi/images/{}/{}.npy'.format(study_id, acq_id)
    if os.path.exists(fn):
        print("Loading existing...")
        try:
            return np.load(fn, allow_pickle=True)
        except:
            pass
    s3_rawbucket = s3.Bucket('zou-lab-data')
    raw_tif = s3_rawbucket.Object(study_id +
                                  '/stitched_image_output/' +
                                  acq_id +
                                  '.ome.tif')

    raw_tif_stream = io.BytesIO()
    raw_tif.download_fileobj(raw_tif_stream)
    raw_tif_stream.seek(0)

    tif_img = tifffile.imread(raw_tif_stream)
    os.makedirs(os.path.dirname(fn), exist_ok=True)
    np.save(fn, tif_img)
    return tif_img

def get_segmask_from_acq_id(study_id, acq_id, version=5):
    fn = '/home/ubuntu/efs/dapi/segmask/{}/{}.npy'.format(study_id, acq_id)
    if os.path.exists(fn):
        print("Loading existing...")
        try:
            return np.load(fn, allow_pickle=True)
        except:
            pass
    s3_rawbucket = s3.Bucket('zou-lab-data')
    raw_tif = s3_rawbucket.Object(study_id +
                                  '/segmentation/'+str(version)+'/masks/' +
                                  acq_id +
                                  '.ome.tif')

    raw_tif_stream = io.BytesIO()
    raw_tif.download_fileobj(raw_tif_stream)
    raw_tif_stream.seek(0)

    tif_img = tifffile.imread(raw_tif_stream)
    os.makedirs(os.path.dirname(fn), exist_ok=True)
    np.save(fn, tif_img)
    return tif_img

def min_max_image(img, vmin=None, vmax=None):
    if vmin is None:
        vmin = 0
    if vmax is None:
        vmax = 1
    return (img - vmin) / (vmax - vmin)

def norm_image(img): 
    const_upper = 5000
    const_lower = 10
    cnts, vals = np.histogram(img, bins=range(0, 2**16, 256))
    pixels = img.shape[0]*img.shape[1] if len(img.shape) == 2 else len(img)
    upper = pixels/const_lower
    lower = pixels/const_upper

    crit = np.where((lower < cnts) & (cnts <= upper))[0]
    th_min = min(crit)
    th_max = max(crit)

    min_val, max_val = vals[th_min], vals[th_max]
    
    img[img < min_val] = min_val
    img[img >= max_val] = max_val

    img = min_max_image(img)
    return img

def pred_to_one_hot(y_pred, length):
    y_pred = np.array(y_pred)
    if len(y_pred.shape) == 3:
        y_pred = y_pred[:,:,1]
        y_pred = np.swapaxes(y_pred, 0, 1)
    y_pred = np.argmax(y_pred, axis=1)
    y_p = []
    for y in y_pred:
        a = np.zeros(length)
        a[y] = 1
        y_p.append(a)
    y_pred = np.array(y_p)
    return y_pred

def get_labels(dataset):
    if dataset == 'upmc':
        labels = ['APC', 'B cell', 'CD4 T cell', 'CD8 T cell', 'Granulocyte', 'Lymph vessel',
              'Macrophage', 'Naive immune cell', 'Stromal / Fibroblast', 'Tumor', 'Tumor (CD15+)',
              'Tumor (CD20+)', 'Tumor (CD21+)', 'Tumor (Ki67+)', 'Tumor (Podo+)', 'Vessel']
    elif dataset == 'charville':
        labels = ['B Cell', 'CD4 T Cells', 'CD8 T Cells', 'Dendritic Cell',
       'Eosinophils', 'Granulocyte', 'Macrophage', 'NK Cell', 'Nerves',
       'Other', 'Stromal Cell', 'Tumor 1', 'Tumor 3',
       'Tumor 4 (Ki67 Proliferative)', 'Tumor 5', 'Vessels']
#         labels = ['B cell', 'Blood vessel', 'CD4 T cell', 'CD8 T cell',
#            'Granulocyte', 'Macrophage', 'Other', 'Stroma', 'Tumor 1',
#            'Tumor 2 (Ki67 Proliferating)', 'Tumor 3', 'Tumor 4', 'Tumor 5',
#            'Tumor 6 / DC', 'Tumor 7']
    elif dataset == 'cshn':
        labels = ['B cells', 'DCs', 'Lymphatics', 'MDSCs', 'Mac', 'Mast cells',
       'Monocytes', 'Muscle', 'NK cells', 'Nerves', 'Neutrophils',
       'Other', 'Stromal', 'Teff', 'Th', 'Treg', 'Tumor', 'Vessels']
    return labels

def get_pred_cols(labels):
    return ['pred_{}'.format(i) for i in range(len(labels))]

def get_biomarkers(dataset):
    if dataset == 'charville':
        biomarkers = ['CD107a', 'CD117', 'CD11b', 'CD11c', 'CD134', 'CD14', 'CD15',
           'CD163', 'CD20', 'CD21', 'CD31', 'CD38', 'CD3e', 'CD4', 'CD45',
           'CD45RA', 'CD45RO', 'CD49f', 'CD68', 'CD8', 'CollagenIV', 'FoxP3',
           'Gal3', 'GranzymeB', 'HLA-DR', 'ICOS', 'Ki67', 'PD1', 'PDL1', 'PGP9.5',
           'PanCK', 'Podoplanin', 'RORgammaT', 'S100A4', 'Siglec8', 'TFAM', 'TIM3',
           'Vimentin', 'aSMA', 'DAPI']
    elif dataset == 'upmc':
        biomarkers = ['CD117', 'CD11b', 'CD11c', 'CD134', 'CD14', 'CD15', 'CD152', 'CD16',
            'CD20', 'CD21', 'CD31', 'CD34', 'CD38', 'CD3e', 'CD4', 'CD45',
           'CD45RA', 'CD45RO', 'CD47', 'CD49f', 'CD56', 'CD57', 'CD68', 'CD69',
           'CD8', 'CollagenIV', 'FoxP3', 'GranzymeB', 'HLA-DR', 'ICOS', 'Ki67',
           'PD1', 'PDL1', 'PanCK', 'Podoplanin', 'TMEM16A', 'Vimentin', 'aSMA',
           'p16', 'DAPI']
            #'CD163',
    elif dataset == 'cshn':
        biomarkers = ['CD107a', 'CD117', 'CD11b', 'CD11c',
           'CD134', 'CD14', 'CD15', 'CD197', 'CD20', 'CD21', 'CD31', 'CD34',
           'CD3e', 'CD4', 'CD45', 'CD45RA', 'CD45RO', 'CD49f', 'CD57', 'CD68',
           'CD8', 'CollagenIV', 'DAPI', 'FoxP3', 'GATA3', 'Gal3', 'GranzymeB',
           'HLA-DR', 'Ki67', 'PD1', 'PDL1', 'PGP9.5', 'PanCK',
           'Podoplanin', 'TFAM', 'TMEM16A', 'Vimentin', 'aSMA', 'p16']
    elif dataset == 'lung_codex':
        biomarkers = ['CD107a', 'CD117', 'CD11b', 'CD11c',
           'CD14', 'CD141', 'CD15', 'CD163', 'CD183', 'CD197', 'CD20', 'CD21',
           'CD25', 'CD31', 'CD34', 'CD38', 'CD3e', 'CD4', 'CD44', 'CD45', 'CD45RA',
           'CD45RO', 'CD56', 'CD68', 'CD8', 'DAPI', 'FoxP3', 'GranzymeB', 'HLA-DR',
           'Ki67', 'PGP9.5', 'PanCK', 'Podoplanin', 'RORgammaT', 'SPP1', 'Siglec8',
           'Vimentin']
    elif dataset == 'lung_7plex':
        biomarkers = ['CD68', 'CD8', 'DAPI', 'FoxP3', 'PD1',
           'PDL1', 'PanCK']
    return biomarkers

def get_morphology_features():
    return ['Orientation.Orientation', 'Size.Area',
       'Size.ConvexHullArea', 'Size.MajorAxisLength', 'Size.MinorAxisLength',
       'Size.Perimeter', 'Shape.Circularity', 'Shape.Eccentricity',
       'Shape.EquivalentDiameter', 'Shape.Extent', 'Shape.MinorMajorAxisRatio',
       'Shape.Solidity', 'Shape.HuMoments1', 'Shape.HuMoments2',
       'Shape.HuMoments3', 'Shape.HuMoments4', 'Shape.HuMoments5',
       'Shape.HuMoments6', 'Shape.HuMoments7']

def get_top_biomarkers(k=7):
    biomarkers = ['DAPI', 'CD45RA', 'CD15', 'PanCK', 'HLA-DR', 'Ki67', 'Vimentin']
    return biomarkers[0:k]

def get_alt_top_biomarkers(k=7):
    biomarkers = ['DAPI', 'CD4', 'CD15', 'PanCK', 'CD8', 'Ki67', 'Vimentin']
    return biomarkers[0:k]

def get_embeddings(run_name, n_dims=10):
    path = '/home/ubuntu/efs/dapi/resnet/dapi/{}/embeddings_DAPI/'.format(run_name)
    combined_file = path+'all.npy'
    if os.path.exists(combined_file):
        print("Loading combined file...")
        embeddings = np.load(combined_file, allow_pickle=True)
    else:
        files = os.listdir(path)
        files = sorted(files, key=lambda x: int(x.split('.')[0]), reverse=False)
        embeddings = []
        for fn in files:
            cur_file = np.load(path+fn, allow_pickle=True)
            embeddings.append(np.vstack(cur_file))
            gc.collect()
        embeddings = np.vstack(embeddings)
        np.save(combined_file, embeddings)

    embeddings = PCA(n_components=n_dims).fit_transform(embeddings)
    return embeddings

def get_embed_cols(n_dims=10):
    return ['embed_{}'.format(x) for x in range(n_dims)]

def get_pred_df(data_version, run_name):
    data_df = pd.read_parquet('/home/ubuntu/efs/dapi/resnet/dfs/version_{}/data_df.pqt'.format(data_version))
    if isinstance(run_name, list):
        preds = []
        for run in run_name:
            preds.append(np.load('/home/ubuntu/efs/dapi/resnet/dapi/{}/preds_all.npy'.format(run)))
        preds = np.array(preds).mean(axis=0)        
    else:
        preds = np.load('/home/ubuntu/efs/dapi/resnet/dapi/{}/preds_all.npy'.format(run_name))
    pred_cols = ['pred_{}'.format(x) for x in range(len(preds[0]))]
    data_df[pred_cols] = preds
    preds_num = [x for x in np.argmax(preds, axis=1)]
    data_df['preds_num'] = preds_num
    return data_df

def binarize_labels(df, labels):
    for i, label in enumerate(labels):
        df[label] = (df['LABEL'] == i).astype(int)
    return df
        
def get_df_splits(merged_df, test_coverslip=None, ovr=False):
    if test_coverslip is not None:
        if ovr:
            train_rows =  merged_df[(merged_df['COVERSLIP_ID'] == test_coverslip)]
            valid_rows = None
            test_rows = merged_df[~(merged_df['COVERSLIP_ID'] == test_coverslip)]
        else:
            train_rows =  merged_df[~(merged_df['COVERSLIP_ID'] == test_coverslip)]
            valid_rows = None
            test_rows = merged_df[(merged_df['COVERSLIP_ID'] == test_coverslip)]
    else:
        train_rows = merged_df[merged_df.split.isin(['train', 'valid'])]
        valid_rows = merged_df[merged_df.split.isin(['valid'])]
        test_rows = merged_df[merged_df.split.isin(['test'])]
    return train_rows, valid_rows, test_rows


def plot_ct_legend(labels):
    cm = matplotlib.cm.get_cmap("tab20")
    node_type_to_color_mapping = {n: cm(i%20) for i, n in enumerate(labels)}
    plt.clf()
    fig = plt.figure(figsize=(1, 1))
    for ct in labels:
        plt.plot([0], [0], '.', label=ct, c=node_type_to_color_mapping[ct])
    plt.plot([0], [0], '.', c='w', markersize=10)
    plt.legend(bbox_to_anchor=(1.05, 1.))
    plt.axis('off')
    plt.show()

def get_dataset_dict():
    dataset_dict = {}
    dataset_dict['hnscc'] = {
        'study_uuid': '1c3702ff-627e-4cb5-a8e1-1eab68aa95bb',
        'biomarker_map': json.load(open('/home/ubuntu/efs/dapi/data/hnscc_map.json')),
        'acq_ids': np.load('/home/ubuntu/efs/dapi/data/hnscc_acq_ids.npy', allow_pickle=True),
        'segmask_version': 1
    }

    dataset_dict['upmc'] = {
        'study_uuid': '196a98d2-f0c1-4f22-b249-701dcbc3da84',
        'biomarker_map': json.load(open('/home/ubuntu/efs/dapi/data/upmc_map.json')),
        'acq_ids': np.load('/home/ubuntu/efs/dapi/data/upmc_acq_ids.npy', allow_pickle=True),
        'segmask_version': 5
    }

    dataset_dict['charville'] = {
        'study_uuid': '5a987988-a392-452f-9e3d-2e5625498768',
        'biomarker_map': json.load(open('/home/ubuntu/efs/dapi/data/charville_map.json')),
        'acq_ids': np.load('/home/ubuntu/efs/dapi/data/charville_acq_ids.npy', allow_pickle=True),
        'segmask_version': 6
    }
    return dataset_dict

def calculate_f1(true=None, preds=None, labels=None):
    return pd.DataFrame.from_dict(
            classification_report(true, preds, target_names=labels, output_dict=True)).T

def calculate_r2(true=None, preds=None, biomarkers=None):
    df_ = []
    for i, biomarker in enumerate(biomarkers):
        score = stats.pearsonr(true[:,i], preds[:,i])[0]
        df_.append([biomarker, score])
    return pd.DataFrame(df_, columns=['Biomarker', 'R2'])

def calculate_exp_f1(true=None, preds=None, biomarkers=None, quantile=0.75):
    df_ = []
    for i, biomarker in enumerate(biomarkers):
        median = np.quantile(true[:,i], quantile)
        score = roc_auc_score(true[:,i]>median, preds[:,i]>median)
        df_.append([biomarker, score])
    return pd.DataFrame(df_, columns=['Biomarker', 'F1'])

def sample_to_patches(acq_id=None, study_id=None, wh=100):
    cell_df = []
    img = get_segmask_from_acq_id(study_id=study_id, acq_id=acq_id)
    min_num_cells = 1
    num_y_tiles = int(np.ceil(img.shape[0]/wh))
    num_x_tiles = int(np.ceil(img.shape[1]/wh))
    tot_num_tiles = num_y_tiles * num_x_tiles

    for i in range(num_y_tiles):
        for j in range(num_x_tiles):
            y1,y2,x1,x2 = [i*wh, (i+1)*wh, j*wh, (j+1)*wh]
            patch = img[y1:y2, x1:x2]
            if patch.shape[0] < wh and patch.shape[1] < wh:
                patch_full = np.zeros((wh, wh))
                patch_full[0:patch.shape[0], 0:patch.shape[1]] = patch
                patch = patch_full
            elif patch.shape[0] < wh:
                patch = np.concatenate([patch, np.zeros((wh-patch.shape[0], wh))], axis=0)
            elif patch.shape[1] < wh:
                patch = np.concatenate([patch, np.zeros((wh, wh-patch.shape[1]))], axis=1)
            cell_nums = np.unique(patch)[1:]
            num_cells = len(cell_nums)
            if num_cells < min_num_cells:
                #print("Not enough cells")
                continue
            [cell_df.append([acq_id+'_'+str(cell_num), i, j, acq_id+'_'+str(i)+'_'+str(j), acq_id]) for cell_num in cell_nums]
    return pd.DataFrame(cell_df, columns=['CELL_ID', 'y', 'x', 'PATCH_ID', 'ACQUISITION_ID'])

def ci(simulations, n, p=0.95):
    """
    Return 2-sided symmetric confidence interval specified
    by p.
    """
    u_pval = (1+p)/2.
    l_pval = (1-u_pval)
    l_indx = int(np.floor(n*l_pval))
    u_indx = int(np.floor(n*u_pval))
    return(np.mean(simulations), np.abs(simulations[l_indx]-simulations[u_indx])/2)


# def patchwise_label_r2(data_df=None, comp_df=None, true_col=None, pred_col=None, cell_labels=None):
#     df = data_df.merge(comp_df, on='CELL_ID')[['CELL_ID', 'PATCH_ID', true_col, pred_col]]
#     scores = []
#     num_labels = len(df[true_col].unique())
#     pred_vecs = []
#     true_vecs = []
#     for patch_id, rows in df.groupby('PATCH_ID'):
#         true_counts = np.unique(rows[true_col], return_counts=True)
#         true_dict = dict(zip(true_counts[0].astype(int), true_counts[1]/len(rows)))
#         true_vec = [true_dict[i] if i in true_dict else 0.0 for i in range(num_labels)]
#         pred_counts = np.unique(rows[pred_col], return_counts=True)
#         pred_dict = dict(zip(pred_counts[0].astype(int), pred_counts[1]/len(rows)))
#         pred_vec = [pred_dict[i] if i in pred_dict else 0.0 for i in range(num_labels)]
#         #pdb.set_trace()
#         scores.append(stats.pearsonr(true_vec, pred_vec)[0])
# #         true_vecs.append(true_vec)
# #         pred_vecs.append(pred_vec)
# #     true_vecs = np.array(true_vecs)
# #     pred_vecs = np.array(pred_vecs)
#     return np.mean(scores)
# #     results_df = [[cell_labels[i], r2_score(true_vecs[:,i], pred_vecs[:,i])] for i in range(len(true_vecs[0]))]
# #     results_df = pd.DataFrame(results_df, columns=['LABEL', 'R2'])
# #     return results_df

# def patchwise_label_f1(data_df=None, comp_df=None, true_col=None, pred_col=None, cell_labels=None):
#     df = data_df.merge(comp_df, on='CELL_ID')[['CELL_ID', 'PATCH_ID', true_col, pred_col]]
#     scores = []
#     num_labels = len(df[true_col].unique())
    
#     for patch_id, rows in df.groupby('PATCH_ID'):
#         true_counts = np.unique(rows[true_col], return_counts=True)
#         true_dict = dict(zip(true_counts[0].astype(int), true_counts[1]))
#         true_vec = [1.0 if i in true_dict else 0.0 for i in range(num_labels)]
#         pred_counts = np.unique(rows[pred_col], return_counts=True)
#         pred_dict = dict(zip(pred_counts[0].astype(int), pred_counts[1]))
#         pred_vec = [1.0 if i in pred_dict else 0.0 for i in range(num_labels)]
#         #pdb.set_trace()
#         scores.append(f1_score(true_vec, pred_vec))
#     return np.mean(scores)

def patchwise_label_f1(data_df=None, comp_df=None, true_col=None, pred_col=None, cell_labels=None, bootstrap=1000):
    df = data_df.merge(comp_df, on='CELL_ID')[['CELL_ID', 'PATCH_ID', true_col, pred_col]]
    scores = []
    num_labels = len(cell_labels)
    
    true_vec = []
    pred_vec = []
    for patch_id, rows in df.groupby('PATCH_ID'):
        true_counts = np.unique(rows[true_col], return_counts=True)
        true_dict = dict(zip(true_counts[0].astype(int), true_counts[1]))
        true_ = [1.0 if i in true_dict else 0.0 for i in range(num_labels)]
        true_vec.append(true_)
        pred_counts = np.unique(rows[pred_col], return_counts=True)
        pred_dict = dict(zip(pred_counts[0].astype(int), pred_counts[1]))
        pred_ = [1.0 if i in pred_dict else 0.0 for i in range(num_labels)]
        pred_vec.append(pred_)
        #pdb.set_trace()
        #scores.append(f1_score(true_vec, pred_vec))
    #pdb.set_trace()
#     try:
#         df_ = pd.DataFrame.from_dict(classification_report(true_vec, pred_vec, target_names=cell_labels, output_dict=True)).T
#     except:
#         pdb.set_trace()
#     return df_['f1-score']
    if bootstrap is not None:
        f1 = bootstrap_(true_vec, pred_vec, func=f1_bootstrap, n=bootstrap)
        return f1
    else:
        try:
            df_ = pd.DataFrame.from_dict(classification_report(true_vec, pred_vec, target_names=cell_labels, output_dict=True)).T
        except:
            pdb.set_trace()
        return df_['f1-score']
    

def patchwise_exp_r2_bootstrap(data_df=None, comp_df=None, true_cols=None, pred_cols=None, bootstrap=1000):
    #comp_df = comp_df[comp_df['CELL_ID'].isin(data_df['CELL_ID'])]
    #data_df['CELL_ID'] = data_df['CELL_ID'].astype(int)
    data_df = data_df.copy()
    comp_df = comp_df.copy()
    df = data_df.merge(comp_df, on='CELL_ID')[['CELL_ID', 'PATCH_ID']+list(true_cols)+list(pred_cols)]

    true_vec = []
    pred_vec = []
    for patch_id, rows in df.groupby('PATCH_ID'):
        true_vec.append(np.mean(rows[true_cols], axis=0))
        pred_vec.append(np.mean(rows[pred_cols], axis=0))
    
    true_vec = np.array(true_vec)
    pred_vec = np.array(pred_vec)
    
    results_df = []
    r2 = bootstrap_(true_vec, pred_vec, func=r2_bootstrap, n=bootstrap)
    return r2
    #results_df.append([biomarker, r2[0], r2[1]])
        #return pd.DataFrame(results_df, columns=['Biomarker', 'R2', 'CI'])

        
        
# TRUE R2 CALCULATION - COMPUTES IT FOR EACH BIOMARKER
def patchwise_exp_r2(data_df=None, comp_df=None, true_cols=None, pred_cols=None):
    comp_df = comp_df[comp_df['CELL_ID'].isin(data_df['CELL_ID'])]
    data_df = data_df.copy()
    comp_df = comp_df.copy()
    
    try:
        data_df['CELL_ID'] = data_df['CELL_ID'].astype(int)
    except:
        pass
    #pdb.set_trace()

    df = data_df.merge(comp_df, on='CELL_ID')[['CELL_ID', 'PATCH_ID']+list(true_cols)+list(pred_cols)]

    true_vec = []
    pred_vec = []
    for patch_id, rows in df.groupby('PATCH_ID'):
        true_vec.append(np.mean(rows[true_cols], axis=0))
        pred_vec.append(np.mean(rows[pred_cols], axis=0))
    
    true_vec = np.array(true_vec)
    pred_vec = np.array(pred_vec)
    
    results_df = []
    for i, biomarker in enumerate(true_cols):
        r2 = stats.pearsonr(true_vec[:,i], pred_vec[:,i])[0]
        results_df.append([biomarker, r2])
        if r2 == 1.0:
            pdb.set_trace()

    return pd.DataFrame(results_df, columns=['Biomarker', 'R2'])    

def bootstrap_(y_true, y_pred, func=None, n=1000, cpus=8):
    simulations = []
    sample_size = len(y_pred)    
    def g():
        for i in range(n):
            yield (y_true, y_pred) 
    pool = multiprocessing.Pool(cpus)
    simulations = pool.map(func, g())
    #pdb.set_trace()
#     for i in range(n):
#         simulations.append(func((y_true, y_pred)))
    simulations.sort()
    return ci(simulations, n=n, p=0.95)

def r2_bootstrap(args):
    y_true, y_pred = args
    #pdb.set_trace()
    iteridx = np.random.choice(range(len(y_pred)), size=len(y_pred), replace=True)
    vals = []
    #pdb.set_trace()
    for i in range(len(y_pred[0])):
        vals.append(stats.pearsonr(np.array(y_pred)[iteridx, i], np.array(y_true)[iteridx, i])[0])
    return np.mean(vals)

def f1_bootstrap(args):
    y_true, y_pred = args
    #pdb.set_trace()
    iteridx = np.random.choice(range(len(y_pred)), size=len(y_pred), replace=True)
    val = f1_score(np.array(y_true)[iteridx], np.array(y_pred)[iteridx], average='weighted')
    #print(val)
    return val
    


def get_adj_mtxs(pred_df, cell_labels, test_cs='c005'):
    pred_dict = pred_df[['CELL_ID', 'PRED']].set_index('CELL_ID').to_dict(orient='index')
    
    adj_mtx_true = np.zeros((len(cell_labels), len(cell_labels)))
    adj_mtx_pred = np.zeros((len(cell_labels), len(cell_labels)))
    k = 0
    j = 0
    prefix = '/home/ubuntu/efs/dapi/data/graph/'
    for fn in os.listdir(prefix):
        if test_cs not in fn: continue
        with open(prefix+fn, "rb") as fp:
            G = pickle.load(fp)
        for i, j in G.edges:
            j += 0
            if G.nodes[i]['cell_type'] == 'Unassigned' or G.nodes[j]['cell_type'] == 'Unassigned':
                continue
            k_true = sorted([cell_labels.index(G.nodes[i]['cell_type']), cell_labels.index(G.nodes[j]['cell_type'])])
            if G.nodes[i]['cell_id'] not in pred_dict or G.nodes[j]['cell_id'] not in pred_dict:
                k += 1
                continue
            k_pred = sorted([pred_dict[G.nodes[i]['cell_id']]['PRED'], pred_dict[G.nodes[j]['cell_id']]['PRED']])
#             except:
#                 i += 1
#                 continue
            adj_mtx_true[k_true[0], k_true[1]] += 1
            adj_mtx_true[k_true[1], k_true[0]] += 1
            adj_mtx_pred[k_pred[0], k_pred[1]] += 1
            adj_mtx_pred[k_pred[1], k_pred[0]] += 1

    print(k, j)     
    row_sums = adj_mtx_true.sum(axis=1)
    row_sums[row_sums == 0] = 1
    adj_mtx_true = adj_mtx_true / row_sums[:, np.newaxis]
    
    row_sums = adj_mtx_pred.sum(axis=1)
    row_sums[row_sums == 0] = 1
    adj_mtx_pred = adj_mtx_pred / row_sums[:, np.newaxis]
    
    return adj_mtx_true, adj_mtx_pred


def plot_codex_graph(G, 
                     unique_node_types=None, 
                     size=None,
                     node_colors=None,
                     node_types=None,
                     cell_types=None,
                     color_mapping=None,
                    ax=None):
    """ Plot nx graph
    """
    # Extract basic node attributes
    node_coords = [node[1]['center_coord'] for node in G.nodes(data=True)]
    node_coords = np.stack(node_coords, 0)
    if size is None:
        x_size = node_coords.max(0)[0] * 1.005
        y_size = node_coords.max(0)[1] * 1.005
        size = (x_size, y_size)
        #plt.clf()
        #if ax is None:
            #plt.figure(figsize=(10, 10*size[1]/size[0]))
        
    if node_colors is None:
        # Define node color by node type
        cm = matplotlib.cm.get_cmap("tab20")
        if color_mapping is None:
            node_type_to_color_mapping = {n: cm(i%20) for i, n in enumerate(cell_types)}
        else:
            node_type_to_color_mapping = color_mapping
        if node_types is None:
            node_types = [node[1]['cell_type'] for node in G.nodes(data=True)]
        colors = [node_type_to_color_mapping[n] for n in node_types]
        #pdb.set_trace()
    else:
        colors = list(node_colors)
        #print(node_coords.shape[0])
        assert len(colors) == node_coords.shape[0]
    
    for (i, j, edge_type) in G.edges.data():
        xi, yi = G.nodes[i]['center_coord']
        xj, yj = G.nodes[j]['center_coord']
        if edge_type['edge_type'] == 'neighbor':
            plotting_kwargs = {"c": "k",
                               "linewidth": 0.5,
                               "linestyle": '-'}
        else:
            plotting_kwargs = {"c": (0.4, 0.4, 0.4, 1.0),
                               "linewidth": 0.1,
                               "linestyle": '--'}
        if ax is None:
            plt.plot([xi, xj], [yi, yj], zorder=1, **plotting_kwargs)
        else:
            ax.plot([xi, xj], [yi, yj], zorder=1, **plotting_kwargs)

    if ax is None:
        plt.scatter(node_coords[:, 0], node_coords[:, 1], s=30, c=colors, zorder=2)
        plt.xlim(0, size[0])
        plt.ylim(0, size[1])
    else:
        ax.scatter(node_coords[:, 0], node_coords[:, 1], s=30, c=colors, zorder=2)
        ax.axis(xmin=0,xmax=size[0],ymin=0,ymax=size[1])
    return


# def bootstrap_f1(y_true, y_pred, n=100):
#     """
#     Generate `n` bootstrap samples, evaluating `func`
#     at each resampling. `bootstrap` returns a function,
#     which can be called to obtain confidence intervals
#     of interest.
#     """
#     simulations = list()
#     sample_size = len(y_pred)
#     num_tried = 0
#     while len(simulations) < n and num_tried < 10000:
#         num_tried += 1
#         iteridx = np.random.choice(range(len(y_pred)), size=sample_size, replace=True)
#         iterypred = [np.array(y_pred)[idx] for idx in iteridx]
#         iterytrue = [np.array(y_true)[idx] for idx in iteridx]
#         if len(set(iterytrue)) < 2:
#             continue
#         simulations.append(roc_auc_score(iterytrue, iterypred))
#     simulations.sort()
#     def ci(p):
#         """
#         Return 2-sided symmetric confidence interval specified
#         by p.
#         """
#         u_pval = (1+p)/2.
#         l_pval = (1-u_pval)
#         l_indx = int(np.floor(n*l_pval))
#         u_indx = int(np.floor(n*u_pval))
#         return(np.mean(simulations), np.abs(simulations[l_indx]-simulations[u_indx])/2)
#     result = ci(.95)
#     return result

def print_results(results_df):
    print('{:.3f} ({:.3f})'.format(results_df[0], results_df[1]))
