from .utils import *
from joblib import dump, load
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
import xgboost as xgb
import cuml
import tensorflow as tf

class CODEXModel():

    def __init__(self, dfs, features):
        train_df, val_df, test_df = dfs
        x_features, y_features = features
        self.x_features, self.y_features = x_features, y_features

        self.X_train = train_df[x_features].astype(np.float32).values
        self.Y_train = train_df[y_features].values
        if val_df is not None:
            self.X_valid = val_df[x_features].astype(np.float32).values
            self.Y_valid = val_df[y_features].values
        if test_df is not None:
            self.X_test = test_df[x_features].astype(np.float32).values
            self.Y_test = test_df[y_features].values

        self.results_df = None
        self.test_preds = None
        self.clf = None
        self.labels = None

    def fit(self):
        self.clf.fit(self.X_train, self.Y_train)

    def predict(self, X_test=None):
        if X_test is None:
            X_test = self.X_test
        self.test_preds = self.clf.predict(X_test)
        return self.test_preds

    def calculate_r2(self, preds=None):
        if preds is not None:
            self.test_preds = preds
        df_ = []
        for i, biomarker in enumerate(self.y_features):
            score = r2_score(self.Y_test[:,i], self.test_preds[:,i])
            df_.append([biomarker, score])
        self.results_df = pd.DataFrame(df_, columns=['Biomarker', 'R2'])
        return self.results_df

    def calculate_f1(self, preds=None, labels=None):
        if preds is not None:
            self.test_preds = preds
        self.results_df = pd.DataFrame.from_dict(
                        classification_report(self.Y_test, self.test_preds, target_names=labels, output_dict=True)).T
        return self.results_df
    
    def save(self, fn):
        dump(self.clf, 'models/{}.joblib'.format(fn))
    
    def load(self, fn):
        pass

    # def get_confusion_matrix(self):
    #     cm = confusion_matrix(Y_test, label_preds, normalize='true')
    #     sns.heatmap(cm.T, xticklabels=[x+" (gt)" for x in cell_labels], yticklabels=cell_labels)
    #     plt.show()

class VanillaRFRegModel(CODEXModel):
    def __init__(self, dfs, features):
        super().__init__(dfs, features)
        self.clf = RandomForestRegressor(n_jobs=-1, max_samples=100000, verbose=1)

class LGBMRegModel(CODEXModel):
    def __init__(self, dfs, features):
        super().__init__(dfs, features)
        self.clf = multioutput.MultiOutputRegressor(lgb.LGBMRegressor(n_jobs=-1))

class MLPRegModel(CODEXModel):

    def __init__(self, dfs, features):
        
        super().__init__(dfs, features)
        self.clf = Sequential()
        self.quantile = 0.9
        
#     def quantile_loss(self, q, y, y_p):
#         e = y-y_p
#         return tf.keras.backend.mean(tf.keras.backend.maximum(q*e, (q-1)*e))

    
    def fit(self):
        self.clf.add(Dense(100, input_shape=(None, self.X_train.shape[1]), activation='tanh'))
        #self.clf.add(Dropout(0.5))
        self.clf.add(Dense(100, activation='tanh'))
        #self.clf.add(Dropout(0.5))
        self.clf.add(Dense(100, activation='tanh'))
        self.clf.add(Dropout(0.5))
        self.clf.add(Dense(self.Y_train.shape[1]))
    
        self.clf.compile(loss='mse', optimizer='adam', metrics=['mse'])
        self.clf.fit(self.X_train, self.Y_train, epochs=10, batch_size=256, verbose=0, shuffle=True)
        import gc; gc.collect()
        keras.backend.clear_session()

    def save(self, fn):
        self.clf.save('models/{}.hdf5'.format(fn))

    def load(self, fn):
        self.clf = keras.models.load_model('models/{}.hdf5'.format(fn))

class VanillaRFClsModel(CODEXModel):
    def __init__(self, dfs, features):
        super().__init__(dfs, features)
        self.clf = cuml.ensemble.RandomForestClassifier()

    def predict(self, X_test=None):
        if X_test is None:
            X_test = self.X_test
        y_pred = self.clf.predict_proba(X_test)
        self.test_preds = np.argmax(y_pred, axis=1)
        return self.test_preds

class XGBoostRegModel(CODEXModel):
    
    def __init__(self, dfs, features):
        super().__init__(dfs, features)
        self.clf = multioutput.MultiOutputRegressor(xgb.XGBRegressor(n_jobs=-1, tree_method='gpu_hist', gpu_id=0,
                                                                    learning_rate=0.1,
                                                                    n_estimators=500,
                                                                     max_depth=3,
                                                                    colsample_bytree=0.7))

    def save(self, fn):
        self.clf.save_model('models/{}.model'.format(fn))

class KNNClsModel(CODEXModel):

    def __init__(self, dfs, features):
        super().__init__(dfs, features)
        self.clf = cuml.neighbors.KNeighborsClassifier(n_neighbors=20)

    def save(self, fn):
        self.clf.save_model('models/{}.model'.format(fn))

    def predict(self, X_test=None):
        if X_test is None:
            X_test = self.X_test
        y_pred = self.clf.predict_proba(X_test)
        self.test_preds = np.argmax(y_pred, axis=1)
        return self.test_preds

class KNNRegModel(CODEXModel):

    def __init__(self, dfs, features):
        super().__init__(dfs, features)
        self.clf = multioutput.MultiOutputRegressor(cuml.neighbors.KNeighborsRegressor(n_neighbors=5))

#     def save(self, fn):
#         self.clf.save_model('models/{}.model'.format(fn))

#     def predict(self, X_test=None):
#         if X_test is None:
#             X_test = self.X_test
#         y_pred = self.clf.predict_proba(X_test)
#         self.test_preds = np.argmax(y_pred, axis=1)
#         return self.test_preds