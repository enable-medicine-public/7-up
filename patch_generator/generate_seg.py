# Create patches and store coordinate info in name
import pandas as pd
import os
import numpy as np
from PIL import Image
import pdb
import multiprocessing
import boto3
import tifffile
import io
import snowflake.connector
import cv2
import argparse
import imageio
import json
from scipy.ndimage.morphology import binary_dilation
import sys
sys.path.append('/home/ubuntu/efs/dapi/Clean/scripts')
from utils import *

def norm_max_min(img): 

    const_upper = 5000
    const_lower = 10
    cnts, vals = np.histogram(img, bins=range(0, 2**16, 256))
    pixels = img.shape[0]*img.shape[1]
    upper = pixels/const_lower
    lower = pixels/const_upper
    # try:
    crit = np.where((lower < cnts) & (cnts <= upper))[0]
    try:
        th_min = min(crit)
        th_max = max(crit)
        min_val, max_val = vals[th_min], vals[th_max]
    except:
        min_val = 0
        max_val = 2**16


    img[img < min_val] = min_val
    img[img >= max_val] = max_val

    img = ((img - np.min(img)) / (np.max(img) - np.min(img)) * 255).astype(np.uint8)
    # except:
    #     #pdb.set_trace()
    return img

def sample_to_segs(acq_id):

    print("Processing sample:", acq_id)
    # make tile save directory
    patch_save_dir = os.path.join(patches_save_dir, acq_id)
    if not os.path.exists(patch_save_dir):
        os.makedirs(patch_save_dir)

    seg_img = get_segmask_from_acq_id(study_uuid, acq_id, segmask_version)
    all_img = get_img_from_acq_id(study_uuid, acq_id)
    # pdb.set_trace()
    # all_img is cycle,channel,height,width
    bm_img = all_img[biomarker_map[biomarker]['cycle']-1,biomarker_map[biomarker]['channel']-1,:,:]
    bm_img = norm_max_min(bm_img)

    # Now, get the cell with the surrounding context
    padded_bm_img = np.zeros((bm_img.shape[0]+ps, bm_img.shape[1]+ps), dtype=bm_img.dtype)
    padded_bm_img[pad:-pad, pad:-pad] = bm_img
    padded_seg_img = np.zeros((seg_img.shape[0]+ps, seg_img.shape[1]+ps), dtype=seg_img.dtype)
    padded_seg_img[pad:-pad, pad:-pad] = seg_img

    for cell_id in np.unique(seg_img):
        if cell_id == 0:
            continue
        if cell_id % 10000 == 0:
            print(cell_id)

        cell_patch_path = os.path.join(patch_save_dir,
                                     '{}.png'.format(
                                         str(cell_id).zfill(3)
                                     )) 
        if os.path.exists(cell_patch_path):
            continue

        # first, get the cell only patch
        if cell_xy is not None:
            df_ = cell_xy[cell_xy['CELL_ID'] == cell_id]
            x,y = df_['X'].values[0], df_['Y'].values[0]

            ctx = padded_bm_img[y:y+ps, x:x+ps]
            ctx = cv2.convertScaleAbs(ctx)
            ctx_2x = ctx[pad//2:3*pad//2, pad//2:3*pad//2] 
            try:
                ctx_2x = cv2.resize(ctx_2x, dsize=(ps, ps), interpolation=cv2.INTER_NEAREST)
            except:
                continue
            seg_patch = padded_seg_img[y:y+ps, x:x+ps]
            binary_patch = (seg_patch == cell_id)
            if np.sum(binary_patch) == 0:
                pdb.set_trace()
                print("ERROR", cell_id)
                continue
            cell_patch = np.multiply(binary_patch, ctx)

            pixels = np.where(binary_patch)
            t,b,l,r = np.min(pixels[0]), np.max(pixels[0]), np.min(pixels[1]), np.max(pixels[1])
            h,w = b-t,r-l
            w *= rf
            h *= rf
            cell = cell_patch[t:b, l:r]
            try:
                cell = cv2.resize(cell, dsize=(w, h), interpolation=cv2.INTER_NEAREST)
            except:
                continue
            
            if h>224 or w>224:
                #print("Too big: {},{}".format(h, w))
                continue
            patch = np.zeros((ps, ps), dtype=cell.dtype)
            o_h, o_w = int(h%2>0), int(w%2>0)
            patch[pad-h//2:pad+h//2+o_h, pad-w//2:pad+w//2+o_w] = cell

            #pdb.set_trace()
        else:
            binary_mask = (seg_img == cell_id)
            # Binary Dilation
            # binary_mask = binary_dilation(binary_mask, iterations=5)
            cell_img = np.multiply(binary_mask, bm_img)
            pixels = np.where(binary_mask)
            t,b,l,r = np.min(pixels[0]), np.max(pixels[0]), np.min(pixels[1]), np.max(pixels[1])
            h,w = b-t,r-l
            y,x = (b+t)//2, (r+l)//2
            w *= rf
            h *= rf
            cell = cell_img[t:b, l:r]
            #pdb.set_trace()

            try:
                cell = cv2.resize(cell, dsize=(w, h), interpolation=cv2.INTER_NEAREST)
            except:
                continue

            if h>224 or w>224:
                #print("Too big: {},{}".format(h, w))
                continue
            patch = np.zeros((ps, ps), dtype=cell.dtype)
            o_h, o_w = int(h%2>0), int(w%2>0)
            patch[pad-h//2:pad+h//2+o_h, pad-w//2:pad+w//2+o_w] = cell
            # Now, get the cell with the surrounding context
            padded_img = np.zeros((seg_img.shape[0]+ps, seg_img.shape[1]+ps), dtype=np.uint8)
            padded_img[pad:-pad, pad:-pad] = bm_img
            ctx = padded_img[y:y+ps, x:x+ps]
            ctx_2x = ctx[pad//2:3*pad//2, pad//2:3*pad//2]
            try:
                ctx_2x = cv2.resize(ctx_2x, dsize=(ps, ps), interpolation=cv2.INTER_NEAREST)
            except:
                continue
        
        combined_patch = Image.fromarray(np.dstack([patch, ctx, ctx_2x]))
        # if len(np.unique(combined_patch)) < 25:
        #    # pdb.set_trace()
        #    continue
        combined_patch.save(cell_patch_path)

    print("Finished acquisition id {}".format(acq_id))

def map(acq_ids):
    num_processes = 4#multiprocessing.cpu_count()
    pool = multiprocessing.Pool(num_processes)
    pool.map(sample_to_segs, acq_ids)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--biomarker', type=str)
    parser.add_argument('--dataset', type=str)
    parser.add_argument('--version', type=str)

    args = parser.parse_args()

    ps = 224
    pad = ps//2
    rf = 3
    version = args.version if args.version is not None else 0
    prefix = '/data/'

    dataset_dict = {}
    dataset_dict['hnscc'] = {
        'study_uuid': '1c3702ff-627e-4cb5-a8e1-1eab68aa95bb',
        'biomarker_map': json.load(open('/home/ubuntu/efs/dapi/data/hnscc_map.json')),
        'acq_ids': np.load('/home/ubuntu/efs/dapi/data/hnscc_acq_ids.npy', allow_pickle=True),
        'segmask_version': 1
    }

    dataset_dict['upmc'] = {
        'study_uuid': '196a98d2-f0c1-4f22-b249-701dcbc3da84',
        'biomarker_map': json.load(open('/home/ubuntu/efs/dapi/data/upmc_map.json')),
        'cell_xy': pd.read_parquet('/home/ubuntu/efs/dapi/Clean/dfs/UPMC/cell_xy.pqt'),
        'acq_ids': np.load('/home/ubuntu/efs/dapi/data/upmc_acq_ids.npy', allow_pickle=True),
        'segmask_version': 5
    }

    dataset_dict['charville'] = {
        'study_uuid': '5a987988-a392-452f-9e3d-2e5625498768',
        'biomarker_map': json.load(open('/home/ubuntu/efs/dapi/data/charville_map.json')),
        'acq_ids': np.load('/home/ubuntu/efs/dapi/data/charville_acq_ids.npy', allow_pickle=True),
        'segmask_version': 6
    }

    dataset_dict['cellsight'] = {
        'study_uuid': 'ea8c0519-5cc6-4d77-af93-9b26ad9f398c',
        'biomarker_map': json.load(open('/home/ubuntu/efs/dapi/data/cellsight_map.json')),
        'acq_ids': np.load('/home/ubuntu/efs/dapi/data/cellsight_acq_ids.npy', allow_pickle=True),
        'segmask_version': 5
    }

    dataset_dict['lung_7plex'] = {
        'study_uuid': '3f8de412-77e5-479c-8dfa-944f07eed1d4',
        'biomarker_map': json.load(open('/home/ubuntu/efs/dapi/data/lung_7plex_map.json')),
        'cell_xy': pd.read_parquet('/home/ubuntu/efs/dapi/Clean/dfs/Lung_7plex/cell_xy.pqt'),
        'acq_ids': np.load('/home/ubuntu/efs/dapi/data/lung_7plex_acq_ids.npy', allow_pickle=True),
        'segmask_version': 2
    }

    dataset_dict['lung_codex'] = {
        'study_uuid': 'f0793e10-45a1-4c43-9748-d9639069de31',
        'biomarker_map': json.load(open('/home/ubuntu/efs/dapi/data/lung_codex_map.json')),
        'cell_xy': pd.read_parquet('/home/ubuntu/efs/dapi/Clean/dfs/Lung_codex/cell_xy.pqt'),
        'acq_ids': np.load('/home/ubuntu/efs/dapi/data/lung_codex_acq_ids.npy', allow_pickle=True),
        'segmask_version': 2
    }

    dataset = args.dataset

    study_uuid = dataset_dict[dataset]['study_uuid']
    biomarker_map = dataset_dict[dataset]['biomarker_map']
    cell_xy = dataset_dict[dataset]['cell_xy'] if 'cell_xy' in dataset_dict[dataset] else None
    acq_ids = dataset_dict[dataset]['acq_ids']
    acq_ids = [x.replace('samp', 'reg') for x in acq_ids]
    segmask_version = dataset_dict[dataset]['segmask_version']
    biomarker = args.biomarker
    print("Generating patches for {}".format(biomarker))
    patches_save_dir = prefix+'segs/{}/{}/version_{}/'.format(dataset.upper(), biomarker, version)
    print(patches_save_dir)
    os.makedirs(patches_save_dir, exist_ok=True)
    # for acq_id in acq_ids:
    #     import time
    #     t1 = time.time()
    #     sample_to_segs(acq_id)
    #     print(time.time() - t1)
    #     #break
    #     print(acq_id)
    map(acq_ids)
    print("Done!")
